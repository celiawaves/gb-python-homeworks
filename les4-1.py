from sys import argv

name, time, salary, bonus = argv
try:
    time = int(time)
    salary = int(salary)
    bonus = int(bonus)
    res = time * salary + bonus
    print(f'З/п сотрудника составляет: {res}')
except ValueError:
    print('Not a number')
